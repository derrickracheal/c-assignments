#include<stdio.h>
#include<math.h>

int main()
{
    float a, b, c, d, r1, r2, r1real, r1img, r2real, r2img;

    printf("Enter a, b, c\n");
    scanf("%f %f %f", &a, &b, &c);
        // Validate input: Ensure that 'a' is not zero
    if (a == 0) {
        printf("Coefficient 'a' must be nonzero. Exiting program.\n");
        return 1;  // Exit with an error code
    }



    d = b * b - 4 * a * c;
    printf("Discriminant: %f ", d);

    if (d > 0)
    {
        // real and distinct roots
        printf("\nRoots are real and distinct");
        r1 = (-b + sqrt(d)) / (2 * a);
        r2 = (-b - sqrt(d)) / (2 * a);
        printf("\nFirst root = %f and second root = %f ", r1, r2);
    } 
    else if (d == 0)
    {
        printf("\nRoots are real and equal");
        r1 = -b / (2 * a);
        r2 = -b / (2 * a);
        printf("\nFirst root = %f and second root = %f ", r1, r2);
    } 
    else 
    {
        printf("\nRoots are complex");
        r1real = -b / (2 * a);
        r1img = sqrt(-d) / (2 * a);
        r2real = -b / (2 * a);
        r2img = sqrt(-d) / (2 * a);
        printf("\nFirst root = %f + i%f and second root = %f - i%f", r1real, r1img, r2real, r2img);
    }

    return 0;
}